<?php

namespace App\Observers;

use App\Helpers\ResponseHelper;
use App\Models\Blogs;
use Exception;

class BlogsObserver
{
     /**
     * Handle the Blogs "created" event.
     *
     * @param  \App\Blogs  $Blogs
     * @return void
     */
    public function created(Blogs $Blogs)
    {
          //Send email to all signed Users for notify
          $this->sendMail();
    }

    /**
     * Handle the Blogs "updated" event.
     *
     * @param  \App\Blogs  $Blogs
     * @return void
     */
    public function updated(Blogs $Blogs)
    {
        //
    }

    /**
     * Handle the Blogs "deleted" event.
     *
     * @param  \App\Blogs  $Blogs
     * @return void
     */
    public function deleted(Blogs $Blogs)
    {
        //
    }

    /**
     * Handle the Blogs "restored" event.
     *
     * @param  \App\Blogs  $Blogs
     * @return void
     */
    public function restored(Blogs $Blogs)
    {
        //
    }

    /**
     * Handle the Blogs "force deleted" event.
     *
     * @param  \App\Blogs  $Blogs
     * @return void
     */
    public function forceDeleted(Blogs $Blogs)
    {
        //
    }

    /*
        function to send bulk emails using queue job and redis 
    */
    public function sendMail()
    {
        try{
            $mail_data = [
                'subject' => "Notification"
            ];
            //call send email job
            $job = (new \App\Jobs\SendEmail($mail_data)); 
            dispatch($job);
        }catch(Exception $e){
            return ResponseHelper::operationFail();
        }
        
    }
}
