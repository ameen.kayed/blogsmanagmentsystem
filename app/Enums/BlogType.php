<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Service()
 * @method static static UnavailableService()
 * @method static static Project()
 */
final class BlogType extends Enum
{
    const Text = 1;
    const Video = 2;
}
