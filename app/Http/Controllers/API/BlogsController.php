<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\blogRequest;
use App\Http\Requests\blogRequestEdit;
use App\Enums\BlogType;
use App\Models\Blogs;
use Illuminate\Support\Facades\Auth;
use App\Helpers\ResponseHelper;
use App\Http\Requests\blogRequestDelete;
use App\Http\Requests\commentRequest;
use App\Models\Comment;
use Exception;
use Illuminate\Support\Facades\Storage;

class BlogsController extends Controller
{

    /*
        function to get blog object using id 
    */
    public function show($id)
    {
        $blog = Blogs::where('id',$id)->first();
        
        if(!$blog)
        {
            return ResponseHelper::dataNotFound();
        }
       
        return ResponseHelper::select($blog);
    }

    /*
        function to get all blogs object
    */
    public function showAll()
    {
        try{
            $blog = Blogs::get()->load('comments');       
            return ResponseHelper::select($blog);
        }
        catch(Exception $e){
            return ResponseHelper::operationFail();
        }
    }

    /*
        function to store blog object  
    */
    public function store(blogRequest $request)
    {
        try{
            $data = $request->all();
        
            //check if blog is Text OR Video to set correct type
            if($data['type'] == BlogType::Text){
                $blog = Blogs::create([
                    'title'=>$data['title'],
                    'details'=> $data['details'],
                    'created_by'=> Auth::user()->id,
                    'type'=> BlogType::Text,
                ]);
    
                return ResponseHelper::operationSuccess();
            }
            else if($data['type'] == BlogType::Video){
                $results = $request->file('details')->store('blogsVideos');
                $blog = Blogs::create([
                    'title'=>$data['title'],
                    'details'=> $results,
                    'created_by'=> Auth::user()->id,
                    'type'=> BlogType::Video,
                ]);
              
                return ResponseHelper::operationSuccess();
            }
            else{
                return ResponseHelper::invalidData();
            }
        }catch(Exception $e){
            return ResponseHelper::insertingFail();
        } 
    }

    /*
        function to update blog object
    */
    public function update(blogRequestEdit $request)
    {
        try{
            $data = $request->all();
            $blog = Blogs::where('id',$data['blog_id'])->first();
            
            //check if blog not exists
            if(!$blog)
            {
                return ResponseHelper::dataNotFound();
            }
    
            //check if blog is Text OR Video to set correct type
            if($blog->type == BlogType::Text){
                
                //just update params get in request not all of object
                if(isset($request->title)){
                    $blog->title = $data['title'];
                }
    
                if(isset($request->details)){
                    $blog->details = $data['details'];
                }
                $blog->save();
                return ResponseHelper::operationSuccess();
            }
            else if($blog->type == BlogType::Video){
    
                //just update params get in request not all of object
                if(isset($request->title)){
                    $blog->title = $data['title'];
                }
    
                if(isset($request->details)){
    
                    //if blog has Video then delete the video of the blog 
                    if(Storage::exists($blog->details)){
                        Storage::delete($blog->details);
                    }
    
                    //add the new video of the blog 
                    $results = $request->file('details')->store('blogsVideos');
                    $blog->details = $results;
                }
                //save blog after update
                $blog->save();
                return ResponseHelper::operationSuccess();
            }
            else{
                return ResponseHelper::invalidData();
            }
        }catch(Exception $e){
            return ResponseHelper::updatingFail();
        } 
        
    }

    /*
        function to delete blog object
    */
    public function delete($id)
    {
        try{
            $blog = Blogs::where('id',$id)->first();
        
            //check if blog not exists
            if(!$blog)
            {
                return ResponseHelper::dataNotFound();
            }
    
            //check if blog type is Video to delete file
            if($blog->type == BlogType::Video){
                //delete the video of the blog 
                if(Storage::exists($blog->details)){
                    Storage::delete($blog->details);
                }
            }
          
            //delete the blog using softdelete 
            $blog->delete();
            return ResponseHelper::delete();

        }catch(Exception $e){
            return ResponseHelper::deletingFail();
        }
        
    }

    

    public function addComment(commentRequest $request)
    {
        try{
            $data = $request->all();
            $blog = Blogs::where('id',$data['blog_id'])->first();
            
            //check if blog not exists
            if(!$blog)
            {
                return ResponseHelper::dataNotFound();
            }

            Comment::create([
                'blog_id' => $blog->id,
                'comment' => $data['comment'],
                'created_by'=> Auth::user()->id,
            ]);

            return ResponseHelper::operationSuccess();

        }catch(Exception $e){
            return ResponseHelper::insertingFail();
        }
        
    }

}
