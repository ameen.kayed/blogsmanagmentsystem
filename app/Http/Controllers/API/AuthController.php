<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\loginRequest;
use App\Http\Requests\registerRequest;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Validator;
   

class AuthController extends Controller
{
    /**
     * Register new user api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(registerRequest $request)
    {   
        try{
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $success['token'] =  $user->createToken('MyApp')->accessToken;   
            $success['user'] =  $user;
    
            return ResponseHelper::operationSuccess($success);
        }catch(Exception $e){
            return ResponseHelper::operationFail();
        }
      
    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(loginRequest $request)
    {
        try{
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
                $user = Auth::user(); 
                $success['token'] =  $user->createToken('MyApp')-> accessToken; 
                $success['user'] =  $user;
       
                return ResponseHelper::operationSuccess($success);
            } 
            else{ 
                return ResponseHelper::operationFail(['error'=>'Unauthorised']);
            } 
        }catch(Exception $e){
            return ResponseHelper::operationFail();
        }        
    }
}
