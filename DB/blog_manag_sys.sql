-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Jul 02, 2022 at 06:59 AM
-- Server version: 8.0.18
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog_manag_sys`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `details`, `created_by`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 15:45:16', '2022-07-01 15:45:16', NULL),
(2, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 15:45:49', '2022-07-01 15:45:49', NULL),
(3, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 15:46:50', '2022-07-01 15:55:31', '2022-07-01 15:55:31'),
(4, 'blog title edit', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 15:48:50', '2022-07-01 15:54:44', '2022-07-01 15:54:44'),
(5, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:07:31', '2022-07-01 16:07:31', NULL),
(6, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:26:37', '2022-07-01 16:26:37', NULL),
(7, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:36:52', '2022-07-01 16:36:52', NULL),
(8, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:36:56', '2022-07-01 16:36:56', NULL),
(9, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:36:59', '2022-07-01 16:36:59', NULL),
(10, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:37:00', '2022-07-01 16:37:00', NULL),
(11, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:37:02', '2022-07-01 16:37:02', NULL),
(12, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:37:03', '2022-07-01 16:37:03', NULL),
(13, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:37:45', '2022-07-01 16:37:45', NULL),
(14, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:40:04', '2022-07-01 16:40:04', NULL),
(15, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:40:44', '2022-07-01 16:40:44', NULL),
(16, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:42:49', '2022-07-01 16:42:49', NULL),
(17, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:44:21', '2022-07-01 16:44:21', NULL),
(18, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:45:44', '2022-07-01 16:45:44', NULL),
(19, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:46:28', '2022-07-01 16:46:28', NULL),
(20, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:46:54', '2022-07-01 16:46:54', NULL),
(21, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:46:58', '2022-07-01 16:46:58', NULL),
(22, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:47:44', '2022-07-01 16:47:44', NULL),
(23, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:48:36', '2022-07-01 16:48:36', NULL),
(24, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:50:51', '2022-07-01 16:50:51', NULL),
(25, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:51:29', '2022-07-01 16:51:29', NULL),
(26, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:51:53', '2022-07-01 16:51:53', NULL),
(27, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:55:08', '2022-07-01 16:55:08', NULL),
(28, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:56:01', '2022-07-01 16:56:01', NULL),
(29, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:57:02', '2022-07-01 16:57:02', NULL),
(30, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 16:58:51', '2022-07-01 16:58:51', NULL),
(31, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 17:00:04', '2022-07-01 17:00:04', NULL),
(32, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 17:02:57', '2022-07-01 17:02:57', NULL),
(33, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 17:03:30', '2022-07-01 17:03:30', NULL),
(34, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 17:35:18', '2022-07-01 17:35:18', NULL),
(35, 'blog title', 'details can be text or vedio file\ntype 1 for text, type 2 for vedio', 7, 1, '2022-07-01 17:37:41', '2022-07-01 17:37:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `comment` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `blog_id`, `comment`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 2, 'test comment', 7, '2022-07-01 16:02:07', '2022-07-01 16:02:07');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(2, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(3, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(4, '2016_06_01_000004_create_oauth_clients_table', 1),
(5, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(6, '2022_07_01_130935_create_users_table', 1),
(7, '2022_07_01_130936_create_password_resets_table', 1),
(8, '2022_07_01_130937_create_failed_jobs_table', 1),
(9, '2022_07_01_130938_create_personal_access_tokens_table', 1),
(10, '2022_07_01_130939_create_blogs_table', 1),
(11, '2022_07_01_171450_create_comments_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('3dfe0d404f9f0e25942c803ae4cbce684bb5c379a6f9165c959f34fe0d2588b925307c976b73b073', 3, 1, 'MyApp', '[]', 0, '2022-07-01 14:35:20', '2022-07-01 14:35:20', '2023-07-01 17:35:20'),
('b6ff49537256e5edfb750273f9446b5e04718da91a83ef35f3d0ef4c3a7068b60db34e5039e566b6', 4, 1, 'MyApp', '[]', 0, '2022-07-01 15:32:59', '2022-07-01 15:32:59', '2023-07-01 18:32:59'),
('31214ee9961b65ce36733fc3ff3f7d6cf2ccd4b64dceb090e13ce4e3514adc3aaa66a0357b85736d', 5, 1, 'MyApp', '[]', 0, '2022-07-01 15:33:26', '2022-07-01 15:33:26', '2023-07-01 18:33:26'),
('eaae4a4712fe993c4f350ac8f585d75e09db5f7d2375d103966beb7977e84e204bb36e53040ee377', 6, 1, 'MyApp', '[]', 0, '2022-07-01 15:35:50', '2022-07-01 15:35:50', '2023-07-01 18:35:50'),
('0d70513f2b8af3e7ffbccebad3e9803824064319adab91d2c1900576965ab67364be16e5b5ded6b3', 5, 1, 'MyApp', '[]', 0, '2022-07-01 15:40:44', '2022-07-01 15:40:44', '2023-07-01 18:40:44'),
('800c6429ba22a9cf5cfb8fa8cd9280a8cd1b0204a436aa7272a4857e0441318590e9120c3d312af3', 5, 1, 'MyApp', '[]', 0, '2022-07-01 15:41:03', '2022-07-01 15:41:03', '2023-07-01 18:41:03'),
('9664e1dcaa1783e3f3a2e6a68e378bd2771208f064910ac2fe7c1abca44beec93cf6142118855b5e', 5, 1, 'MyApp', '[]', 0, '2022-07-01 15:41:30', '2022-07-01 15:41:30', '2023-07-01 18:41:30'),
('40921234ad660fe23dfc28e3b2393034028d7031805cd09bf70b079ef922afd5949a45bc02829fe4', 7, 1, 'MyApp', '[]', 0, '2022-07-01 15:42:50', '2022-07-01 15:42:50', '2023-07-01 18:42:50'),
('c33e0668688e80f8c73239ddbf0585258e75b21edfc07a59ac5c1de07764f9a4dd70e9c115736f84', 5, 1, 'MyApp', '[]', 0, '2022-07-01 16:13:55', '2022-07-01 16:13:55', '2023-07-01 19:13:55'),
('5ca39823107ac0cc8a9c81fa363540eb7f4b02192ffe4f7e3e5cefd69f3e6077e2c02ee36cb72f95', 5, 1, 'MyApp', '[]', 0, '2022-07-01 16:18:34', '2022-07-01 16:18:34', '2023-07-01 19:18:34'),
('fc35d39d05c385eb4337b9941edab235a5888abc69b0d04cb65812fb50be270ac151020fe2d1e1b1', 5, 1, 'MyApp', '[]', 0, '2022-07-01 16:19:45', '2022-07-01 16:19:45', '2023-07-01 19:19:45'),
('5b7f22d1bb5a8d8f4b23ba46bdc8e534e18f018a711de3687006781ed100b9182dba46f9c21c7d99', 5, 1, 'MyApp', '[]', 0, '2022-07-01 16:20:12', '2022-07-01 16:20:12', '2023-07-01 19:20:12'),
('7ce91dab3a47a64e635270db55275298a84d2c28cd169c2bd02dbc4b08bdc3411e7703287dc985a7', 5, 1, 'MyApp', '[]', 0, '2022-07-01 16:20:34', '2022-07-01 16:20:34', '2023-07-01 19:20:34'),
('a06e1e0ed482e3a69c3559db229eb48b4f139c152f38374518ff3f78e2a192dc609deab5fa8b7f31', 5, 1, 'MyApp', '[]', 0, '2022-07-01 16:22:02', '2022-07-01 16:22:02', '2023-07-01 19:22:02'),
('627edb0b93d34d3ca86affbfaf3f72c70af214b2ed659267c9f8cecd99728ea708b6f47821715016', 8, 1, 'MyApp', '[]', 0, '2022-07-01 16:53:24', '2022-07-01 16:53:24', '2023-07-01 19:53:24'),
('06bd08903a2063377cf1a61852b859b8d0de149f5c891061536f2d4c8e587c7a2a3ea950fec0493b', 9, 1, 'MyApp', '[]', 0, '2022-07-01 16:53:27', '2022-07-01 16:53:27', '2023-07-01 19:53:27'),
('c7a8e606c5391b98772cd6a4abb48543bd3d8ac1355d7d91832fc35304377f59944876375ae11337', 10, 1, 'MyApp', '[]', 0, '2022-07-01 16:53:31', '2022-07-01 16:53:31', '2023-07-01 19:53:31'),
('8e6bb99f2ff9907fa3b3a0bb5169d95006386d70ddcc8cfdb1622a3d8b4f3dc611b0aed099a29a08', 11, 1, 'MyApp', '[]', 0, '2022-07-01 16:53:36', '2022-07-01 16:53:36', '2023-07-01 19:53:36'),
('3dfe12794f0d1282a2dfbc197108014b4240b7a2a4d2cdebfbc759089d2baac33e4409c8eb08401a', 12, 1, 'MyApp', '[]', 0, '2022-07-01 16:53:39', '2022-07-01 16:53:39', '2023-07-01 19:53:39'),
('4217344e54705eed4fa2f3c6b3e4a2e27c750e3a8093681179df584245d5bf626c374f132eb4e72b', 13, 1, 'MyApp', '[]', 0, '2022-07-01 16:53:43', '2022-07-01 16:53:43', '2023-07-01 19:53:43'),
('0f1f4dd92f2c3dc9b62bd36e25fd9f7bc79e15064619efe0a0cd1d69862ec998e5bae59abe54f8dc', 14, 1, 'MyApp', '[]', 0, '2022-07-01 16:53:46', '2022-07-01 16:53:46', '2023-07-01 19:53:46'),
('4986488c393af710e65dab9d134d3a8563e9dc5c1165a58dc45aa682e3dd4dcce18fa8079cb0e9ca', 15, 1, 'MyApp', '[]', 0, '2022-07-01 16:53:50', '2022-07-01 16:53:50', '2023-07-01 19:53:50'),
('1cf516fdd71a2952c72f492982a9a0b4680817812a1731c4f21131f8cf7081dde182e638847aa8cd', 16, 1, 'MyApp', '[]', 0, '2022-07-01 16:53:53', '2022-07-01 16:53:53', '2023-07-01 19:53:53'),
('b7add444d8f0f5252081fbaeef0cab16bcbf6aab60ebaa1ac2bae9c240c0d7a1c6b6a3091c113185', 17, 1, 'MyApp', '[]', 0, '2022-07-01 16:53:58', '2022-07-01 16:53:58', '2023-07-01 19:53:58'),
('0755e512d67de3b03070b4e930c4268fff4957580660462c03db09b98de1961312841a18def1d8eb', 18, 1, 'MyApp', '[]', 0, '2022-07-01 16:54:02', '2022-07-01 16:54:02', '2023-07-01 19:54:02'),
('12f22e54872b38e2ba6b375d44a647f8fdf988690dd80617eb6d74874b9df462c2f1ec6d20089cd3', 19, 1, 'MyApp', '[]', 0, '2022-07-01 16:54:06', '2022-07-01 16:54:06', '2023-07-01 19:54:06'),
('bef119433d973ba950538f878bafe478816924bef4da866915fae9fe2e860fa3b68b32c6fdf972f0', 20, 1, 'MyApp', '[]', 0, '2022-07-01 16:54:09', '2022-07-01 16:54:09', '2023-07-01 19:54:09'),
('40ac50ca77c5fec4e53f5d116053239aab04a36a474fd8a356496b07252306708bb0d9d231c9fb3c', 21, 1, 'MyApp', '[]', 0, '2022-07-01 16:54:14', '2022-07-01 16:54:14', '2023-07-01 19:54:14'),
('3206e9a5e732a25c1bf26326fc4c959ee2867c348c1de1972b4d608e44921f7a36f90f1356a92d01', 22, 1, 'MyApp', '[]', 0, '2022-07-01 16:54:18', '2022-07-01 16:54:18', '2023-07-01 19:54:18'),
('8e26845ac65b18390ddb0fa68ef3db41d81ccd769bbb69c9f7f97938d0479ce9428f4933edfe87cf', 23, 1, 'MyApp', '[]', 0, '2022-07-01 16:54:22', '2022-07-01 16:54:22', '2023-07-01 19:54:22'),
('8dce15478075755aa4e3fa59673cba4604051c82310aa1a0802aec422def44e216ee3c57b689b159', 24, 1, 'MyApp', '[]', 0, '2022-07-01 16:54:25', '2022-07-01 16:54:25', '2023-07-01 19:54:25'),
('4c6c8d4bc4ba7aa20c0489cf11ffa3e6146881e0b2ed475d9dcc5ec1083389acfc71b6b02c986d3f', 25, 1, 'MyApp', '[]', 0, '2022-07-01 16:54:31', '2022-07-01 16:54:31', '2023-07-01 19:54:31'),
('ff33e572325ab48dd2780aa2a229ee3360d974d0c8d13e9e21aecee505833857ea45694b44aa533b', 26, 1, 'MyApp', '[]', 0, '2022-07-01 16:54:34', '2022-07-01 16:54:34', '2023-07-01 19:54:34'),
('f1c5423301ec9c84721c9c4ae2750d5592a08dcf669d00e6c27f680ae7db28df04851f07287a4119', 27, 1, 'MyApp', '[]', 0, '2022-07-01 16:54:41', '2022-07-01 16:54:41', '2023-07-01 19:54:41'),
('92c170f307179f27037c8ce9090c0f46c92e91877b76c36d5ee43a092e28e53cdabbeafd8889763f', 28, 1, 'MyApp', '[]', 0, '2022-07-01 16:54:44', '2022-07-01 16:54:44', '2023-07-01 19:54:44'),
('d324d19a011676dca802718d068539c86eaf9d68d0506ed90fa1bd49c090017707a847286657f1d3', 29, 1, 'MyApp', '[]', 0, '2022-07-01 16:54:48', '2022-07-01 16:54:48', '2023-07-01 19:54:48'),
('ebfe333f93ce3886e22baca53e5d1e584dba067d27321a9ce602c711a75d38c28ec9e833113be356', 30, 1, 'MyApp', '[]', 0, '2022-07-01 16:54:52', '2022-07-01 16:54:52', '2023-07-01 19:54:52'),
('9f50179148f5b32b847dde5230a857d49eb2d7f43654c2a072b0f506ae38ffabddb663e3fbb6e5d8', 5, 1, 'MyApp', '[]', 0, '2022-07-01 17:43:14', '2022-07-01 17:43:14', '2023-07-01 20:43:14'),
('a67eb06d435164289a68ea43134c0c9ac7ddbab2e6db376df4e4cf5a9f079de1ecb89e201d5f1653', 5, 1, 'MyApp', '[]', 0, '2022-07-01 17:51:08', '2022-07-01 17:51:08', '2023-07-01 20:51:08');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'auth', 'PuC5nx1j0qCshfjCciVcofiiEMvtfMe2odv1cje1', NULL, 'http://localhost', 1, 0, 0, '2022-07-01 14:35:06', '2022-07-01 14:35:06');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2022-07-01 14:35:06', '2022-07-01 14:35:06');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'ameen', 'amkyd.888550@gmail.com', NULL, '$2y$10$DLQ6qQFE3QpaLtKpa7vsPuxCYfiw6XCFfkxloOL906vijcT502fJ.', NULL, '2022-07-01 14:34:34', '2022-07-01 14:34:34', NULL),
(3, 'ameen', 'amkyd.dd@gmail.com', NULL, '$2y$10$lHxTUty279sO0GrHuGpIp.sgEUy7iTnpsxdvGB3Uh2E6HbHTZAIV.', NULL, '2022-07-01 14:35:20', '2022-07-01 14:35:20', NULL),
(4, 'ameen', 'amkyd.dsssd@gmail.com', NULL, '$2y$10$9AGEIdrdxMYyxXydyRVmdurjIW7ISUZ/Ko2zngp2ad.//XMPnulQ6', NULL, '2022-07-01 15:32:58', '2022-07-01 15:32:58', NULL),
(5, 'TestUser', 'test@test.test', NULL, '$2y$10$xx6ArZ0UVYMg5ANZJwpUQu8uiBxc/VbCmtL9MTCWdmJuDL1KLJpZW', NULL, '2022-07-01 15:33:26', '2022-07-01 15:33:26', NULL),
(6, 'TestUser', 'test@test.testx', NULL, '$2y$10$I5IFpn3I/sE8nxc6XqmKEeqwz9NVsO9PeZZKBPvYmS7Z7R7xIdZ5y', NULL, '2022-07-01 15:35:50', '2022-07-01 15:35:50', NULL),
(7, 'test', 'test2@test.test', NULL, '$2y$10$BdCaaE0M.70o8XGUYEgggOQcU2AiDPh4we3Z0we4xxWllg3TvzW/u', NULL, '2022-07-01 15:42:50', '2022-07-01 15:42:50', NULL),
(8, 'test', 'test2@test.test1', NULL, '$2y$10$L1m2avDGuf6o1eN9ktrIn.p/k37XkgGPw0iXeTntxGCS6nJpOEA0y', NULL, '2022-07-01 16:53:24', '2022-07-01 16:53:24', NULL),
(9, 'test', 'test2@test.test2', NULL, '$2y$10$tGXRDnazzry49kqG4brO9eWnaOPeCy6.Yc.fUQLntzEiEgVs42g6K', NULL, '2022-07-01 16:53:27', '2022-07-01 16:53:27', NULL),
(10, 'test', 'test2@test.test3', NULL, '$2y$10$ZKu1MbnOgZPTyaQRoGxrEORYibUM7Wjykjxwg/lZ8S874NzVMJpl2', NULL, '2022-07-01 16:53:31', '2022-07-01 16:53:31', NULL),
(11, 'test', 'test2@test.test4', NULL, '$2y$10$qXeiDZLn1CzcbSRUX19Kiuf5BQM1HuXhUoVonfAV3d3eLk0wWQNz6', NULL, '2022-07-01 16:53:36', '2022-07-01 16:53:36', NULL),
(12, 'test', 'test2@test.test5', NULL, '$2y$10$5l5flw6n/US.627HdrRkCOm3FQj6YqspIQhe2zCI634tAXvKU7lg.', NULL, '2022-07-01 16:53:39', '2022-07-01 16:53:39', NULL),
(13, 'test', 'test2@test.test6', NULL, '$2y$10$SAXroPDWaRQCYPz.P7suNunmd5kEASopFpwJn.mPyazZJAx8aZ8w2', NULL, '2022-07-01 16:53:43', '2022-07-01 16:53:43', NULL),
(14, 'test', 'test2@test.test7', NULL, '$2y$10$c/ZsA9yQvWW/SjCo1Dvr0.qHomgsXw1EgfIjvoVFrhM9DKIjOQxPe', NULL, '2022-07-01 16:53:46', '2022-07-01 16:53:46', NULL),
(15, 'test', 'test2@test.test73', NULL, '$2y$10$s0ADYLCNriwvjmio2gqg2.yCDlbPmsE4z0sDse.s5/kH1Fw.uJBSG', NULL, '2022-07-01 16:53:50', '2022-07-01 16:53:50', NULL),
(16, 'test', 'test2@test.test73234', NULL, '$2y$10$sT/DaqpB8kivrUKuTuhHDOa4T5/r4yQBaQ2XVBkjOOdrDkNPXT73.', NULL, '2022-07-01 16:53:53', '2022-07-01 16:53:53', NULL),
(17, 'test', 'test2@test.wewr', NULL, '$2y$10$8AGZbBkwaYZ2VG3N5EcmY.7bNobkJgwOqZ9YWCmIUHyqUNYpgul7y', NULL, '2022-07-01 16:53:58', '2022-07-01 16:53:58', NULL),
(18, 'test', 'test2@test.34234', NULL, '$2y$10$iJsEgIkPByzo1lIymeZH3.vSVYcTReHFreuv2Z3jr4e1ixqYntaNW', NULL, '2022-07-01 16:54:02', '2022-07-01 16:54:02', NULL),
(19, 'test', 'test2@test.sfgf', NULL, '$2y$10$FYW/HbK.5Hwr3rXV.7pLRuc14k5AikEM.QIfbSm6EMkZJb3MAqixe', NULL, '2022-07-01 16:54:06', '2022-07-01 16:54:06', NULL),
(20, 'test', 'test2@test.ghfd', NULL, '$2y$10$rL16ev9UrSrXoCk0mK1Lg.DSwv0tdPxVo9TMu/xxY7XT69bnOyCGS', NULL, '2022-07-01 16:54:09', '2022-07-01 16:54:09', NULL),
(21, 'test', 'test2@test.sdfgfd', NULL, '$2y$10$rVJtCqhKVwIn2Z5YG4vNjOJnSY7yO1/UdivWAEBzxm4BCnCGrzqqO', NULL, '2022-07-01 16:54:14', '2022-07-01 16:54:14', NULL),
(22, 'test', 'test2@test.jhg', NULL, '$2y$10$JJ2GGWv3wgA5iisI1jST/.W0vaGjcogqwuBDlW76rLjjX4.QtIRGK', NULL, '2022-07-01 16:54:18', '2022-07-01 16:54:18', NULL),
(23, 'test', 'test2@test.654', NULL, '$2y$10$BzMO2MGqUUDdNf/bv0lX2OAXHFFYoHf39UQqb77Y3EUIQlm6I5zAu', NULL, '2022-07-01 16:54:22', '2022-07-01 16:54:22', NULL),
(24, 'test', 'ttt@r.com', NULL, '$2y$10$DsGqqmASIAATSDOk23udc.fJejr8mXBY6RkzJkcmX0ruqT//4ri3.', NULL, '2022-07-01 16:54:25', '2022-07-01 16:54:25', NULL),
(25, 'test', 'werwer@test.dsfgdfg', NULL, '$2y$10$Wq.3fQUneExSU79q8/v8J./vzoSh1zfm0vgVl4Ug9h90UCNW7/aiu', NULL, '2022-07-01 16:54:31', '2022-07-01 16:54:31', NULL),
(26, 'test', 'sdf@test.vvv', NULL, '$2y$10$ghFtdXE9aUx536TmHKfq0e6grzE0tlyHJDFguaJBnXA8/cy2SJ50O', NULL, '2022-07-01 16:54:34', '2022-07-01 16:54:34', NULL),
(27, 'test', 'test2@test.dsfdf', NULL, '$2y$10$MW1Alr9UIy4ilwGrgMov2uppGeLAD2dl8mgcB7oQ5C7vh87TVQ5pm', NULL, '2022-07-01 16:54:41', '2022-07-01 16:54:41', NULL),
(28, 'test', 'test2@test.ewrdf', NULL, '$2y$10$uvwuCsFj0rOZXYdLadXzT.sy.vF/MrGwS4Mr3SrY1EYrCS141a.v2', NULL, '2022-07-01 16:54:44', '2022-07-01 16:54:44', NULL),
(29, 'test', 'test2@test.htfgh', NULL, '$2y$10$6VNNkGkLK3lxlmVMeLyf1uydpVAf/lGVZxY5gh5XT3TDcfCO.B6D6', NULL, '2022-07-01 16:54:48', '2022-07-01 16:54:48', NULL),
(30, 'test', 'test2@test.sdfgfdg', NULL, '$2y$10$Zv70FxVtvBjXptFmgcDn0O42NA4raRoLUKBOPiIvlfnI0CIwcL8Lu', NULL, '2022-07-01 16:54:52', '2022-07-01 16:54:52', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
