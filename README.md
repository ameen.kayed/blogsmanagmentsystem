## Name

Blog Managment System.

## Description

A blog management system with 2 types of blog:
First type is a text containing title, details and date of creation. 
Second one is a video blog containing  title, video and the date of creation. 
User can add an article. 

## Installation
##### need php version >= 8.0 
##### 1- Clone the reposetory.
##### 2- cd blogManagmentSystem.
##### 3- configure .env file :
######     * database connection.
######     * mail settings.
######     * add this line "QUEUE_DRIVER=redis" to .env file.
##### 4- run "composer install".
##### 5- run "php artisan key:generate".
##### 6- run "php artisan migrate".
##### 7- run "php artisan passport:client --personal".
##### 8- run "php artisan serv".

## Note
##### you can find APIs Postman collections in API folder.
