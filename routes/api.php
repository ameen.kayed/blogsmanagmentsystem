<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\Api\BlogsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
     
//adding auth and cors middleware for security
Route::middleware(['auth:api','cors'])->group(function () {
    //add new blog
    Route::post('store',[BlogsController::class, 'store']);
    //update blog data
    Route::post('update',[BlogsController::class, 'update']);
    //delete blog
    Route::get('delete/{id}',[BlogsController::class, 'delete']);
    //get all blogs with comments
    Route::get('showAll',[BlogsController::class, 'showAll']);
    //show one blog by id
    Route::get('show/{id}',[BlogsController::class, 'show']);
    //add comment to blog
    Route::post('addComment',[BlogsController::class, 'addComment']);
});